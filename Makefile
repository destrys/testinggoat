build: .virtualenv
	. .virtualenv/bin/activate && pip install -r requirements.txt

.virtualenv:
	python -m venv .virtualenv

clean:
	rm -rf .virtualenv
