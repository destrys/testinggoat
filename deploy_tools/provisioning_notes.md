Provisioning Notes
===

## Required Packages
* nginx
* python v?
* virtualenv & pip
* git

e.g. on Ubuntu:
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python
sudo apt install nginx

## nginx config
* see nginx.template.conf
* replace DOMAIN with e.g. superlists-staging.destrysaul.com

## systemd
* see gunicorn-systemd.template.service
* replace DOMAIN with e.g. superlists-staging.destrysaul.com

## Folder structure:
assuming  we have a user account at /home/username:

/home/username/sites/domain/.env
/home/username/sites/domain/db.sqlite3
/home/username/sites/domain/$(git_repo)
/home/username/sites/domain/virtualenv