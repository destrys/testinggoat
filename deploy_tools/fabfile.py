import os
import random
import subprocess

from fabric import task

# run with:
# fab deploy -H {user}@{host} -i {ssh_key_path}
#

REPO_URL = 'https://gitlab.com/destrys/testinggoat.git'


def exists(c, path):
    return c.run(f'test -e "$(echo {path})"', warn=True).ok


@task
def deploy(c):
    site_folder = f'/home/{c.user}/sites/{c.host}'
    c.run(f'mkdir -p {site_folder}')
    with c.cd(site_folder):
        _get_latest_source(c)
        _update_virtualenv(c)
        _create_or_update_dotenv(c)
        _update_static_files(c)
        _update_database(c)
    _restart_gunicorn(c)


def _get_latest_source(c):
    if exists(c, '.git'):
        c.run('git fetch')
    else:
        c.run(f'git clone {REPO_URL} .')
    batcmd = 'git log -n 1 --format=%H'
    current_commit = subprocess.check_output(batcmd, shell=True).strip().decode()
    c.run(f'git reset --hard {current_commit}')


def _update_virtualenv(c):
    if not exists(c, 'virtualenv/bin/pip'):
        c.run('python3.10 -m venv virtualenv')
    c.run('./virtualenv/bin/pip install -r requirements.txt')


def _create_or_update_dotenv(c):
    c.run('touch .env')
    current_contents = c.run('cat .env').stdout
    if 'DJANGO_DEBUG_FALSE' not in current_contents:
        c.run('echo DJANGO_DEBUG_FALSE=y >> .env')
    if 'SITENAME' not in current_contents:
        c.run(f'echo SITENAME={c.host} >> .env')
    if 'DJANGO_SECRET_KEY' not in current_contents:
        new_secret = ''.join(random.SystemRandom().choices(
            'abcdefghijklmnopqrstuvwxyz0123456789', k=50))
        c.run(f'echo DJANGO_SECRET_KEY={new_secret} >> .env')
    if 'EMAIL_PASSWORD' not in current_contents:
        c.run(f'echo EMAIL_PASSWORD={os.environ["EMAIL_PASSWORD"]} >> .env')


def _update_static_files(c):
    c.run('./virtualenv/bin/python manage.py collectstatic --noinput')


def _update_database(c):
    c.run('./virtualenv/bin/python manage.py migrate --noinput')


def _restart_gunicorn(c):
    c.run(f'sudo systemctl restart gunicorn-{c.host}')
