from fabric import Connection


def _get_manage_dot_py(host):
    return f'~/sites/{host}/virtualenv/bin/python ~/sites/{host}/manage.py'


def reset_database(host):
    manage_dot_py = _get_manage_dot_py(host)
    Connection(f'{host}').run(f'{manage_dot_py} flush --no-input')


def _get_server_env_vars(c, host):
    env_lines = c.run(f'cat ~/sites/{host}/.env').stdout.splitlines()
    return dict(line.split('=') for line in env_lines if line)


def create_session_on_server(host, email):
    manage_dot_py = _get_manage_dot_py(host)
    c = Connection(f'{host}', inline_ssh_env=True)
    env_vars = _get_server_env_vars(c, host)
    session_key = c.run(f'{manage_dot_py} create_session {email}', env=env_vars)
    return session_key.stdout.strip()
